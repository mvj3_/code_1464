//declared as final to be able to reference it in inner class declartations of the handlers 
     final AlertDialog.Builder builder=new AlertDialog.Builder(this);
     builder.setTitle("Alert Dialog");
     builder.setMessage("This is the alert's body");
     builder.setIcon(android.R.drawable.ic_dialog_alert);
     
     builder.setPositiveButton("OK", new OnClickListener() {
   
   @Override
   public void onClick(DialogInterface dialog, int which) {
    TextView txt=(TextView)findViewById(R.id.txt);
    txt.setText("You clicked Ok");
   }
  });
     
     builder.setNegativeButton("Cancel", new OnClickListener() {
   
   @Override
   public void onClick(DialogInterface dialog, int which) {
    // TODO Auto-generated method stub
    TextView txt=(TextView)findViewById(R.id.txt);
    txt.setText("You clicked Cancel");
   }
  });
     
     builder.setNeutralButton("Do something", new OnClickListener() {
   
   @Override
   public void onClick(DialogInterface dialog, int which) {
    // TODO Auto-generated method stub
    TextView txt=(TextView)findViewById(R.id.txt);
    txt.setText("Neutral Button Clicked");
    AlertDialog ad=builder.create();
    ad.cancel();
   }
  });
     
     builder.setOnCancelListener(new OnCancelListener() {
   
   @Override
   public void onCancel(DialogInterface dialog) {
    // TODO Auto-generated method stub
    TextView txt=(TextView)findViewById(R.id.txt);
    txt.setText(txt.getText()+" the cancel listner invoked");
   }
  });
     
     
     
     builder.show();
